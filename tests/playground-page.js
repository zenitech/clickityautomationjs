import { Selector, t } from 'testcafe';

class Playground {
    constructor() {
        this.header = Selector('h1').withExactText('Your playground');
        this.icon = Selector('.mainGrid .icon');
        this.requirements = Selector('h2').withExactText('Requirements of the application under test');
        this.sources = Selector('h2').withExactText('Sources');
        this.task = Selector('h2').withExactText('Task');
    }

    async clickOuterPerimeter() {
        const icons = this.icon;
        const iconsCount = await icons.count;
        const size = Math.sqrt(iconsCount);

        for (let index = 0; index < iconsCount; index++) {
            if (index < size || index % size === 0 || index % size === size - 1 || index > size * (size - 1)) {
                await t.click(icons.nth(index));
                //await this.IconColorValidate(icons.nth(index),rgb(255, 27, 58));
            }
        }
    }

    async IconColorValidate(icon, color) {
        await t.expect(icon.getStyleProperty('color')).eql(color);
    }
}

export default new Playground();
